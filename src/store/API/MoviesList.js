import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { BASE_URL } from "../../config";

export const moviesApi = createApi({
    reducerPath: "moviesApi",
    baseQuery: fetchBaseQuery({
        baseUrl: BASE_URL,
    }),
    endpoints: (builder) => ({
        getAllMovies: builder.query({
            query: () => "3/movie/popular?api_key=f73467bb5e4f576d9e4db4f33c287a9e&language=en-US",
        })
    })
});

export const { useGetAllMoviesQuery } = moviesApi;