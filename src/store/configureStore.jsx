import { configureStore } from "@reduxjs/toolkit";
import movieReducer from './movieListSlice'
import { moviesApi } from "./API/MoviesList";

const store = configureStore({
    reducer: {
        movies: movieReducer,
        [moviesApi.reducerPath]: moviesApi.reducer,
    },
    middleware: (movies) => movies().concat(moviesApi.middleware),
});

export default store;