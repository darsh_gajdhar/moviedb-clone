const { createSlice, createAsyncThunk } = require('@reduxjs/toolkit');

export const STATUSES = Object.freeze({
    IDLE: 'idle',
    ERROR: 'error',
    LOADING: 'loading',
});

export const fetchMovies = createAsyncThunk(
    'movies/fetchMovies',
    async () => {
        return fetch('https://api.themoviedb.org/3/movie/popular?api_key=f73467bb5e4f576d9e4db4f33c287a9e&language=en-US').then((response) => response.json())
    }
);

const moviesSlice = createSlice({
    name: 'movies',
    initialState: {
        data: [],
        status: STATUSES.IDLE,
    },
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(fetchMovies.pending, (state) => {
                state.status = STATUSES.LOADING;
            })

            .addCase(fetchMovies.fulfilled, (state, action) => {
                state.status = STATUSES.IDLE;
                state.data = action.payload.results
            })

            .addCase(fetchMovies.rejected, (state) => {
                state.status = STATUSES.ERROR
            })
    }
});

export default moviesSlice.reducer;