import { Box, List } from "@mui/material";
import { styled } from "@mui/system";

const HomeHeroSectionComponent = styled(Box, {
    name: 'HomeHeroSectionComponent'
})({
    marginTop: '15px',
    '@media (min-width: 0px) and (max-width: 768px)': {
        overflow: 'hidden'
    }
});

export const HomeHeroSectionWrapper = styled(Box, {
    name: 'HomeHeroSectionWrapper'
})({
    margin: '0 96px',
    boxSizing: 'border-box',
    '@media (min-width: 0px) and (max-width: 768px)': {
        display: 'flex',
        flexDirection: 'column',
        margin: '0',
        textAlign: 'center'
    }
});

export const HomeHeroSectionWrapperDiv = styled(Box, {
    name: 'HomeHeroSectionWrapperDiv'
})({
    display: 'flex',
    margin: '15px 0px',
    '@media (min-width: 0px) and (max-width: 768px)': {
        display: 'flex',
        flexDirection: 'column',
        margin: '0'
    }
})

export const HomeHeroSectionWrapperLeft = styled(Box, {
    name: 'HomeHeroSectionWrapperLeft'
})({
    display: 'flex',
    width: '25%',
    flexDirection: 'column',
    marginRight: '30px',
    '@media (min-width: 0px) and (max-width: 768px)': {
        width: '80%',
        justifyContent: 'center',
        marginRight: '0'
    }

});

export const HomeHeroSectionList = styled(List, {
    name: 'HomeHeroSectionList'
})({
    border: '1px solid #e3e3e3',
    borderRadius: '10px ',
    marginTop: '15px',
    boxShadow: '0 2px 8px rgba(0,0,0,0.1)',
    padding: ' 0px 20px',
    '@media (min-width: 0px) and (max-width: 768px)': {
        width: '100%',
        marginBottom: '10px',
        marginLeft: '10px',
    }
})

export const HomeHeroSectionWrapperRight = styled(Box, {
    name: 'HomeHeroSectionWrapperRight'
})({
    width: '100%',
    '@media (min-width: 0px) and (max-width: 768px)': {
        width: '95%',
        marginBottom: '10px',

    }
})

export default HomeHeroSectionComponent;