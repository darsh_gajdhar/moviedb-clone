import { Typography, ListItem, ListItemIcon, ListItemText, Stack, Grid } from '@mui/material';
import React from 'react';
import HomeHeroSectionCard from '../../common/HomeHeroSectionCard/HomeHeroSectionCard';
import HomeHeroSectionComponent, { HomeHeroSectionWrapper, HomeHeroSectionWrapperLeft, HomeHeroSectionList, HomeHeroSectionWrapperRight, HomeHeroSectionWrapperDiv } from './style';

const HomeHeroSection = ({ data }) => {
    return (
        <HomeHeroSectionComponent>
            <HomeHeroSectionWrapper>
                <Typography variant="h5">Popular Movies</Typography>
                <HomeHeroSectionWrapperDiv>
                    <HomeHeroSectionWrapperLeft>
                        <Stack spacing={2}>
                            <HomeHeroSectionList>
                                <ListItem >
                                    <ListItemText primary={"Sort"} />
                                    <ListItemIcon>{">"}</ListItemIcon>
                                </ListItem>
                            </HomeHeroSectionList>
                        </Stack>
                        <Stack spacing={2}>
                            <HomeHeroSectionList>
                                <ListItem >
                                    <ListItemText primary={"Filters"} />
                                    <ListItemIcon>{">"}</ListItemIcon>
                                </ListItem>
                            </HomeHeroSectionList>
                        </Stack>
                        <Stack spacing={2}>
                            <HomeHeroSectionList>
                                <ListItem >
                                    <ListItemText primary={"Where To Watch"} />
                                    <ListItemIcon>{">"}</ListItemIcon>
                                </ListItem>
                            </HomeHeroSectionList>
                        </Stack>
                    </HomeHeroSectionWrapperLeft>
                    <HomeHeroSectionWrapperRight>
                        <Grid container rowSpacing={2} columnSpacing={3}>
                            {data.map((movies) => {
                                return (
                                    <Grid item key={movies.id} lg={2.25} xs={12}>
                                        <HomeHeroSectionCard
                                            image={`https://image.tmdb.org/t/p/original${movies.backdrop_path}`}
                                            title={movies.title}
                                            date={movies.release_date}
                                        />
                                    </Grid>
                                )
                            })}
                        </Grid>
                    </HomeHeroSectionWrapperRight>
                </HomeHeroSectionWrapperDiv>
            </HomeHeroSectionWrapper>
        </HomeHeroSectionComponent>
    )
}

export default HomeHeroSection