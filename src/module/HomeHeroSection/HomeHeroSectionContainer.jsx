// import { useDispatch, useSelector } from 'react-redux';
// import { fetchMovies, STATUSES } from '../../store/movieListSlice';
import React from 'react';
import HomeHeroSection from './HomeHeroSection';
import { useGetAllMoviesQuery } from '../../store/API/MoviesList';

const HomeHeroSectionContainer = () => {

    // const dispatch = useDispatch();

    // const { data, status } = useSelector((state) => state.movies);

    // useEffect(() => {
    //     dispatch(fetchMovies());
    // }, [dispatch]);

    // if (status === STATUSES.LOADING) {
    //     return <h2>Loading....</h2>
    // }

    // if (status === STATUSES.ERROR) {
    //     return <h2>Error....</h2>
    // }

    const { data, isLoading, isError } = useGetAllMoviesQuery();

    if (isLoading) {
        return <h2>Loading...</h2>
    }

    if (isError) {
        return <h2>Error...</h2>
    }

    return (
        <HomeHeroSection data={data.results} />
    )
}

export default HomeHeroSectionContainer;