import React from 'react'
import HomeHeroSectionContainer from '../HomeHeroSection/HomeHeroSectionContainer';

const Home = () => {
    return (
        <HomeHeroSectionContainer />
    )
}

export default Home;