import { BrowserRouter, Routes, Route } from 'react-router-dom'
import Home from './module/Home/Home';
import Navbar from './common/Navbar/Navbar';
import { Provider } from 'react-redux';
import store from './store/configureStore'

function App() {
  return (

    <Provider store={store}>

      <BrowserRouter>
        <Navbar />
        <Routes>
          <Route path='/' element={<Home />} />
        </Routes>
      </BrowserRouter>
    </Provider>

  );
}

export default App;
