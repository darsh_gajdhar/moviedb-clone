import React from 'react'
import HomeHeroSectionCardComponent, { HomeHeroSectionCardTitle, HomeHeroSectionCardDate, HomeHeroSectionCardContents } from './style'

const HomeHeroSectionCard = ({ image, title, date }) => {
    return (
        <HomeHeroSectionCardComponent>
            <img src={image} alt={title} />
            <HomeHeroSectionCardContents>
                <HomeHeroSectionCardTitle>
                    {title}
                </HomeHeroSectionCardTitle>
                <HomeHeroSectionCardDate>
                    {date}
                </HomeHeroSectionCardDate>
            </HomeHeroSectionCardContents>
        </HomeHeroSectionCardComponent>
    )
}

export default HomeHeroSectionCard