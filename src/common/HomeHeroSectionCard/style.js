import { Box, Typography } from "@mui/material";
import { styled } from "@mui/system";

const HomeHeroSectionCardComponent = styled(Box, {
    name: 'HomeHeroSectionCardComponent'
})({
    display: 'flex',
    flexDirection: 'column',
    height: '370px',
    borderRadius: '10px',
    boxShadow: '0 2px 8px rgba(0,0,0,0.1)',
    backgroundPosition: '50% 50%',
    backgroundRepeat: 'no-repeat',
    'img': {
        backgroundOrigin: 'border-box',
        width: '100%',
        height: '75%',
        backgroundSize: 'cover',
        borderRadius: '10px',
        marginBottom: '15px',
        objectFit: 'cover'
    },
    '@media (min-width: 0px) and (max-width: 768px)': {
        overflow: 'hidden',
        marginLeft: '13px',
    }
});

export const HomeHeroSectionCardContents = styled(Box, {
    name: 'HomeHeroSectionCardContents'
})({
    margin: ' 0px 12px'
})

export const HomeHeroSectionCardTitle = styled(Typography, {
    name: 'HomeHeroSectionCardContents'
})({

    fontWeight: 'bold',
    fontSize: '14.7px',
    marginBottom: '3px'

})

export const HomeHeroSectionCardDate = styled(Typography, {
    name: 'HomeHeroSectionCardContents'
})({

    fontSize: '12.7px',
    color: 'rgba(0, 0, 0, 0.6)'
})

export default HomeHeroSectionCardComponent;