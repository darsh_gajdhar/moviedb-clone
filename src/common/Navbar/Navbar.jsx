import React from 'react';
import { Link } from 'react-router-dom'
import NavbarComponent, { NavbarWrapper, NavbarWrapperContents, NavbarWrapperLeft, NavbarWrapperRight } from './style';
import AddSharpIcon from '@mui/icons-material/AddSharp';
import GTranslateSharpIcon from '@mui/icons-material/GTranslateSharp';
import SearchSharpIcon from '@mui/icons-material/SearchSharp';

const Navbar = () => {
    return (
        <NavbarComponent>
            <NavbarWrapper>
                <NavbarWrapperLeft>
                    <img src="https://www.themoviedb.org/assets/2/v4/logos/v2/blue_short-8e7b30f73a4020692ccca9c88bafe5dcb6f8a62a4c6bc55cd9ba82bb2cd95f6c.svg" alt='Logo' />
                    <NavbarWrapperContents>
                        Movies
                    </NavbarWrapperContents>
                    <NavbarWrapperContents>
                        TV Shows
                    </NavbarWrapperContents>
                    <NavbarWrapperContents>
                        People
                    </NavbarWrapperContents>
                    <NavbarWrapperContents>
                        More
                    </NavbarWrapperContents>
                </NavbarWrapperLeft>
                <NavbarWrapperRight>
                    <Link to='/'>
                        <AddSharpIcon />
                    </Link>
                    <Link to='/'>
                        <GTranslateSharpIcon />
                    </Link>
                    <Link to='/'>
                        Login
                    </Link>
                    <Link to='/'>
                        Join TMDB
                    </Link>
                    <Link to='/'>
                        <SearchSharpIcon color='primary' />
                    </Link>
                </NavbarWrapperRight>
            </NavbarWrapper>
        </NavbarComponent>
    )
}

export default Navbar