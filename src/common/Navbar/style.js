import { styled } from "@mui/system";
import { Box, Typography } from "@mui/material";

const NavbarComponent = styled(Box, {
    name: 'NavbarComponent'
})({
    height: '65px',
    backgroundColor: 'rgba(3, 37, 65, 1)',
    '@media (min-width: 0px) and (max-width:768px)': {
        height: 'auto',
        overflow: 'hidden'
    }
});

export const NavbarWrapper = styled(Box, {
    name: 'NavbarWrapper'
})({
    margin: '0 96px',
    color: 'white',
    display: 'flex',
    justifyContent: 'space-between',
    fontWeight: 'bold',
    paddingTop: '16px',

    '@media (min-width: 0px) and (max-width:768px)': {
        margin: '0px',
        flexDirection: 'column'
    }
})

export const NavbarWrapperLeft = styled(Box, {
    name: 'NavbarWrapperLeft'
})({
    display: 'flex',
    alignItems: 'center',
    'img': {
        marginRight: '15px',
        height: '19px'
    },
    '@media (min-width: 0px) and (max-width:768px)': {
        marginBottom: '5px'
    }
});

export const NavbarWrapperContents = styled(Typography, {
    name: 'NavbarWrapperContents'
})({
    color: 'white',
    marginRight: '25px',
    cursor: 'pointer',
    fontWeight: 'bold',
    fontSize: '14px',
});

export const NavbarWrapperRight = styled(Box, {
    name: 'NavbarWrapperRight'
})({
    display: 'flex',
    fontWeight: 'bold',
    fontSize: '14px',
    alignItems: 'center',
    justifyContent: 'space-between',
    'a': {
        marginRight: '25px',
        textDecoration: 'none',
        color: 'white'
    },
    '@media (min-width: 0px) and (max-width: 768px)': {
        'a': {
            marginRight: '0'
        }
    }
});

export default NavbarComponent;